import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {WellnessJSON} from "../wellness-json";

@Injectable({
  providedIn: 'root'
})
export class WellnessApiService {

  constructor(private httpClient: HttpClient) {
  }

  getConnection(destinationFrom: string, destionationTo: string): Observable<string> {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    headers.set('Accept', 'application/json');
    return this.httpClient.get<string>('http://transport.opendata.ch/v1/connections?from=' + destinationFrom + '&to=' + destionationTo, {headers: headers})
  }

  getWellness(): Observable<WellnessJSON> {
    const headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origin', '*');
    headers.set('Access-Control-Allow-Methods', 'GET,PUT,OPTIONS');
    headers.set('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Content-Type, Accept, Accept-Language, Origin, User-Agent');
    headers.set('Content-Type', 'application/json');
    headers.set('Accept', 'application/json');
    return this.httpClient.get<WellnessJSON>('http://localhost:4200/api', {headers: headers});
  }
}
