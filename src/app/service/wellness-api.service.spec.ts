import { TestBed } from '@angular/core/testing';

import { WellnessApiService } from './wellness-api.service';

describe('WellnessApiService', () => {
  let service: WellnessApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WellnessApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
