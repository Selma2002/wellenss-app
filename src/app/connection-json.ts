export interface ConnectionJson {
  json
}

export function getNumberTransfers(connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].transfer;
}

export function getDuration(connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].duration;
}

export function getDepartureStationName(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].departure.station.name;
}

export function getDepartureTime(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].departure.departure;
}

export function getDeparturePlatform(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].departure.platform;
}

export function getArrivalStationName(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].arrival.station.name;
}

export function getArrivalTime(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].arrival.arrival;
}

export function getArrivalPlatform(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].arrival.platform;
}

export function getJourneyName(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].journey ?
    connectionJson.json.connections[0].sections[idx].journey.name : null;
}

export function getJourneyNumber(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].journey ?
    connectionJson.json.connections[0].sections[idx].journey.number : null;
}

export function getJourneyTo(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].journey ?
    connectionJson.json.connections[0].sections[idx].journey.to: null;
}

export function getWalkTime(idx: number, connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections[idx].walk ?
    connectionJson.json.connections[0].sections[idx].walk.duration: null;
}

export function getSectionLength(connectionJson: ConnectionJson) {
  return connectionJson.json.connections[0].sections.length;
}

