import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {
  ConnectionJson, getArrivalPlatform,
  getArrivalStationName, getArrivalTime, getDeparturePlatform, getDepartureStationName, getDepartureTime,
  getDuration, getJourneyName, getJourneyNumber, getJourneyTo,
  getNumberTransfers,
  getSectionLength, getWalkTime
} from "../connection-json";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

export interface Connections {
  transfers: number;
  duration: string;
  sections: Section[];
}

export interface Section {
  arrivalStationName: string;
  arrivalPlatform: string;
  arrivalTime: string;
  departureStationName: string;
  departurePlatform: string;
  departureTime: string;
  journeyName: string;
  journeyTo: string;
  journeyNumber: string;
  walk: number;
}

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.scss']
})
export class ConnectionsComponent implements OnInit {
  connectionJson: Observable<ConnectionJson>;
  connection: Connections = {} as Connections;
  sections: Section[] = [];

  constructor(public activatedRoute: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit(): void {
    this.connectionJson = this.activatedRoute.paramMap
      .pipe(map(() => window.history.state));


    this.connectionJson.subscribe(data => {
      console.log(data, 'json')
      this.connection.duration = getDuration(data);
      this.connection.transfers = getNumberTransfers(data);
      const length = getSectionLength(data);
      for (let i = 0; i < length; i++) {
        const section: Section = {
          arrivalStationName: getArrivalStationName(i, data),
          arrivalPlatform: getArrivalPlatform(i, data),
          arrivalTime: getArrivalTime(i, data),
          departureStationName: getDepartureStationName(i, data),
          departurePlatform: getDeparturePlatform(i, data),
          departureTime: getDepartureTime(i, data),
          journeyName: getJourneyName(i, data),
          journeyTo: getJourneyTo(i, data),
          journeyNumber: getJourneyNumber(i, data),
          walk: getWalkTime(i, data),
        };
        this.sections.push(section);
      }
    });

    console.log(this.sections);
  }


  back() {
    this.router.navigateByUrl('/overview')
  }
}
