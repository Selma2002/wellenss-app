import {Component, OnInit} from '@angular/core';
import {WellnessApiService} from "../service/wellness-api.service";
import {
  getAddress,
  getAddressLocality, getLength,
  getName,
  getPictureURL,
  getPostalCode,
  getText,
  WellnessJSON
} from "../wellness-json";
import {MatDialog} from "@angular/material/dialog";
import {ConnectionDialogComponent} from "../connection-dialog/connection-dialog.component";
import {Router} from "@angular/router";

export interface WellnessOption {
  name: string;
  text: string;
  address: string;
  pictureURL: string;
}

export interface DialogData {
  wellnessOption: WellnessOption;
}

@Component({
  selector: 'app-wellness-overview',
  templateUrl: './wellness-overview.component.html',
  styleUrls: ['./wellness-overview.component.scss']
})
export class WellnessOverviewComponent implements OnInit {
  wellnessJson: WellnessJSON;
  wellnessOption: WellnessOption[] = [];
  wellness: string;
  index = 0;

  constructor(private wellnessApiService: WellnessApiService,
              public dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('wellnessOptions')) {
      this.wellnessOption = JSON.parse(localStorage.getItem('wellnessOptions'));
    } else {
      this.wellnessApiService.getWellness().subscribe(value => {
        this.wellnessJson = value;
        this.getWellnessOptions()
      });
    }
  }

  getWellnessOptions() {
    if (this.index < getLength(this.wellnessJson)) {
      const wellnessOption: WellnessOption = {
        name: getName(this.index, this.wellnessJson),
        text: getText(this.index, this.wellnessJson),
        address: getAddress(this.index, this.wellnessJson) + ' ' + getPostalCode(this.index, this.wellnessJson) + ' ' + getAddressLocality(this.index, this.wellnessJson),
        pictureURL: getPictureURL(this.index, this.wellnessJson)
      };
      this.wellnessOption.push(wellnessOption);
      this.index++;
      this.getWellnessOptions();
    }
    localStorage.setItem('wellnessOptions', JSON.stringify(this.wellnessOption));
  }

  getConnection(option: WellnessOption) {
    const dialogRef = this.dialog.open(ConnectionDialogComponent, {
      data: {wellnessOption: option}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigateByUrl('/connections', { state: { json: result } })
      }
    });
  }
}
