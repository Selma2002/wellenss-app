import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellnessOverviewComponent } from './wellness-overview.component';

describe('WellnessOverviewComponent', () => {
  let component: WellnessOverviewComponent;
  let fixture: ComponentFixture<WellnessOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WellnessOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellnessOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
