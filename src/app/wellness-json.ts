export interface WellnessJSON {
  length
}

export function getName(wellnessIdx: number, wellnessJson: WellnessJSON) {
 return wellnessJson[wellnessIdx].name.de;
}

export function getText(wellnessIdx: number, wellnessJson: WellnessJSON) {
  return wellnessJson[wellnessIdx].description.de;
}

export function getAddress(wellnessIdx: number, wellnessJson: WellnessJSON) {
  return wellnessJson[wellnessIdx].address.streetAddress;
}

export function getPictureURL(wellnessIdx: number, wellnessJson: WellnessJSON) {
  return wellnessJson[wellnessIdx].image.url;
}

export function getPostalCode(wellnessIdx: number, wellnessJson: WellnessJSON) {
  return wellnessJson[wellnessIdx].address.postalCode;
}

export function getAddressLocality(wellnessIdx: number, wellnessJson: WellnessJSON) {
  return wellnessJson[wellnessIdx].address.addressLocality;
}

export function getLength(wellnessJSON: WellnessJSON) {
  return wellnessJSON.length;
}
