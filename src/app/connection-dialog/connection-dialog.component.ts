import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DialogData} from "../wellness-overview/wellness-overview.component";
import {WellnessApiService} from "../service/wellness-api.service";

@Component({
  selector: 'app-connection-dialog',
  templateUrl: './connection-dialog.component.html',
  styleUrls: ['./connection-dialog.component.scss']
})
export class ConnectionDialogComponent implements OnInit {
  address = new FormControl('', Validators.required);

  constructor(public dialogRef: MatDialogRef<ConnectionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private wellnessApiService: WellnessApiService) { }

  ngOnInit(): void {
  }

  searchConnections() {
    this.wellnessApiService.getConnection(this.address.value, this.data.wellnessOption.address).subscribe(conn => {
      this.dialogRef.close(conn);
    })
  }
}
