import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WellnessOverviewComponent} from "./wellness-overview/wellness-overview.component";
import {ConnectionsComponent} from "./connections/connections.component";


const routes: Routes = [
  { path: 'overview', component: WellnessOverviewComponent },
  { path: 'connections', component: ConnectionsComponent },
  { path: '',   redirectTo: '/overview', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
